/*
 * Copyright (c) 2014, Christian Gehring
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Autonomous Systems Lab, ETH Zurich nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL  Christian Gehring BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
/*
 * KraftHighLevelController.hpp
 *
 *  Created on: Apr 27, 2015
 *      Author: Christian Gehring, Gabriel Hottiger
 */

#pragma once


#include <std_msgs/Float64MultiArray.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Wrench.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <eigen_conversions/eigen_msg.h>


// highlevel controller
#include "kraft_highlevel_controller/ControllerRos.hpp"
#include "kraft_highlevel_controller/Model.hpp"
#include "kraft_highlevel_controller/InverseKinematic.hpp"

// nodewrap
#include "roscpp_nodewrap/NodeImpl.h"
#include "roscpp_nodewrap/Nodelet.h"

// kindr
#include <kindr/rotations/RotationEigen.hpp>
#include <kindr/rotations/RotationDiffEigen.hpp>
#include <kindr/phys_quant/PhysicalQuantitiesEigen.hpp>

// msgs
#include "kraft_msgs/KraftActuatorCommands.h"
#include "kraft_msgs/KraftState.h"
#include "kraft_msgs/GripperBasePose.h"
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Quaternion.h>

// ros
#include <ros/ros.h>

// system includes
#include <memory>
#include <mutex>

#include <tf/transform_datatypes.h>
#include <Eigen/Geometry>
#include <Eigen/Core>

namespace kraft_highlevel_controller {

class KraftHighLevelController: public nodewrap::NodeImpl {
public:
	KraftHighLevelController();
	virtual ~KraftHighLevelController();
  virtual void init();
  void initializeSubscribers();

protected:

  virtual void cleanup();

protected:
  void updateKraftControllerAndPublish(const kraft_msgs::KraftState::ConstPtr& kraftStatemsg);
  void publish();


  void kraftJoystickCallback(const sensor_msgs::Joy::ConstPtr& msg);
  void kraftStateCallback(const kraft_msgs::KraftState::ConstPtr& msg);
  void GripperBaseReferenceCallback(const kraft_msgs::GripperBaseReference::ConstPtr& msg);

  void initializeMessages();
  void initializeServices();
  void initializePublishers();


 private:

  double timeStep_;
  bool useInverseKinematic_;
  bool gripperBaseReferenceInput_;
  bool moveKraftPosition_;
  bool moveKraftOrientation_;
  bool* inverseKinematicBehavior_;

  tf::Quaternion w_q_EE_;
  Eigen::Vector3d w_euler_EE_;
  Eigen::Vector3d w_r_EE_;
  tf::Quaternion w_q_EEref_;
  Eigen::Vector3d w_euler_EEref_;
  Eigen::Vector3d w_r_EEref_;

  model::Model model_;
  InverseKinematic* inverseKinematic_;

  ros::Subscriber kraftStateSubscriber_;
  ros::Subscriber joystickSubscriber_;
  ros::Subscriber gripperBaseSub_;
  ros::Subscriber gripperBaseReferenceSub_;
  ros::Publisher kraftActuatorCommandsPublisher_;
  ros::Publisher gripperBasePub_;

  kraft_msgs::KraftActuatorCommandsPtr kraftActuatorCommands_;

  std::mutex mutexKraftActuatorCommands_;
  std::mutex mutexKraftJoystick_;
  std::mutex mutexKraftModelAndControllerManager_;
  std::mutex mutexKraftUpdateControllerAndPublish_;
};

} /* namespace kraft_highlevel_controller */
