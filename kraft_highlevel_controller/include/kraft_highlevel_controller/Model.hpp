/*
 * Copyright (c) 2014, Christian Gehring
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Autonomous Systems Lab, ETH Zurich nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL  Christian Gehring BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
/*!
 * @file    Model.hpp
 * @author  Christian Gehring, Gabriel Hottiger
 * @date    Oct, 2014
 */

#pragma once

#include "kraft_model/typedefs.hpp"

#include <iostream>


// custom msgs
#include "kraft_msgs/KraftActuatorCommands.h"
#include "kraft_msgs/KraftState.h"

// kraft model
#include "kraft_model/State.hpp"
#include "kraft_model/Command.hpp"
#include "romo/RobotState.hpp"
#include "kraft_model/typedefs.hpp"

#include "romo/RobotState.hpp"
#include "romo_rbdl/RobotModelRbdl.hpp"
#include "kraft_model/typedefs.hpp"
#include "kraft_description/enums/enums.hpp"
#include "kraft_model/KraftModel.hpp"

#include <iostream>


// tf
#include "tf/transform_datatypes.h"
#include <tf/transform_broadcaster.h>

// eigen
#include <Eigen/Core>

// ros
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Pose.h>

// system includes
#include <memory>

namespace model {

class Model
{
 public:
  Model();
  virtual ~Model();

  kraft_model::KraftModel* getModel();

  kraft_model::State& getState();
  kraft_model::Command& getCommand();

  const kraft_model::State& getState() const;
  const kraft_model::Command& getCommand() const;

  void initializeForController(double dt);

  void reinitialize(double dt);
  void addVariablesToLog();

  void setKraftState(const kraft_msgs::KraftState::ConstPtr& kraftStatemsg);

  void initializeKraftState(kraft_msgs::KraftState& kraftState) const;
  void initializeKraftJointState(sensor_msgs::JointState& kraftjointState) const;

  void getKraftActuatorCommands(kraft_msgs::KraftActuatorCommandsPtr& kraftactuatorCommandsMsg);
  void setKraftJoystickCommands(const sensor_msgs::Joy::ConstPtr& msg);

 private:
  ros::Time updateStamp_;
  std::shared_ptr<kraft_model::KraftModel> kraftModel_;
  kraft_model::State kraftstate_;
  kraft_model::Command kraftcommand_;
  romo::Position r_BasePlate_;
  romo::RotationQuaternion q_BasePlate;
};

} /* namespace model */
