/*
 * Copyright (c) 2015, Jan Preisig
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Autonomous Systems Lab, ETH Zurich nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL  Gabriel Hottiger BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 * InverseKinematics.hpp
 *
 *  Created on: Mai 5, 2016
 *      Author: Jan Preisig
 */

#pragma once

// roco
#include "roco/controllers/Controller.hpp"

// eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

// kraft model
#include "kraft_model/State.hpp"
#include "kraft_model/Command.hpp"
#include "kraft_model/typedefs.hpp"
#include "kraft_model/KraftModel.hpp"

// for calculation with quat and vectors
#include "geometry_msgs/Vector3.h"
#include "geometry_msgs/Pose.h"
#include "tf/transform_datatypes.h"
#include <tf/transform_broadcaster.h>

// kraft msgs
#include "kraft_msgs/GripperBaseReference.h"
#include "kraft_msgs/KraftState.h"

#include "ros/ros.h"

#include "robotUtils/math/LinearAlgebra.hpp"

// system includes
#include <memory>

namespace kraft_highlevel_controller{

class InverseKinematic: 	public 	roco::controllers::Controller<kraft_model::State, kraft_model::Command>
{
public:
using Base = roco::controllers::Controller<kraft_model::State, kraft_model::Command>;
using Vector6d = Eigen::Matrix<double, 6, 1>;

  //! Constructor
  InverseKinematic();

  //! Destructor
  ~InverseKinematic();

  //! Roco implementation
  virtual bool create(double dt);					//returns true
  virtual bool initialize(double dt){return true;}				//executes loadParameters()
  virtual bool reset(double dt);					//
  virtual bool advance(double dt);
  virtual bool change();
  virtual bool cleanup();


  void getGripperBaseState(Eigen::Vector3d& gripperBasePosHL, tf::Quaternion& gripperBaseQuatHL);
  void setGripperBaseReferenceState(Eigen::Vector3d& gripperBaseReferencePosHL, tf::Quaternion& gripperBaseReferenceQuattf_, bool* inverseKinematicBehavior);
  Eigen::Matrix<double,4,3> xiMatrix();
  Eigen::Matrix<double,3,3> skewMatrix(Eigen::Vector3d x);

  //! Parameter loading
  void setParameterPath(std::string path);								//not used
  bool loadParameters();												//not used

private:
  //! Position References
  kraft_model::JointPositions kraftjointPositionReferences_;
  std::shared_ptr<kraft_model::KraftModel> kraftModel_;

  // Vector and Quaternion coming from the manual controller or rqt to set the desired position of the Gripper Base
	Eigen::Vector3d w_r_ref;
  Eigen::Quaterniond w_q_ref;

  // Vector and Quaternion calculated in the inverse kinematic with the kraft model
	Eigen::Vector3d w_r_EE;
  Eigen::Quaterniond w_q_EE;

  // Rotation matrix of the orientation of the Gripper Base to calculate EEQuat and the xiMatrix
  Eigen::Matrix3d w_EulerRotMat;

//  used to set Reference state equal the actual Gripper state to have 0 velocity at the beginning
	bool gripperBaseReferenceInput_;

// used to set v and w equal to 0, when the position or orientation should not change
	bool moveKraftPosition_;
	bool moveKraftOrientation_;

// used if the orientation should stay vertical
	bool stayVertical_;
};

}

