/*
* Copyright (c) 2014, Christian Gehring
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Autonomous Systems Lab, ETH Zurich nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL  Christian Gehring BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
/*
 * KraftHighLevelController.cpp
 *
 *  Created on: Apr 27, 2015
 *      Author: Christian Gehring, Gabriel Hottiger
 */

// highlevel controller
#include "kraft_highlevel_controller/KraftHighLevelController.hpp"
#include "kraft_highlevel_controller/ControllerRos.hpp"

// description
#include "kraft_description/kraft_actuator_commands.hpp"

// kraft model
#include "kraft_model/kraft/kraft.hpp"

// actuator
#include "kraft_actuator/ConvertRosMessages.hpp"

// msgs
#include "kraft_msgs/KraftActuatorCommands.h"

// ros
#include <pluginlib/class_list_macros.h>
#include <ros/callback_queue.h>
#include <ros/package.h>

// system includes
#include <chrono>
#include <cstdint>
#include <string>


namespace kraft_highlevel_controller {

KraftHighLevelController::KraftHighLevelController():
    timeStep_(0.01),
    useInverseKinematic_(true),
    model_(),
    inverseKinematic_(),
    gripperBaseReferenceInput_(false),
    moveKraftPosition_(true),
    moveKraftOrientation_(true),
    inverseKinematicBehavior_(new bool[4])
{
  inverseKinematicBehavior_[0] = false;
  inverseKinematicBehavior_[1] = true;
  inverseKinematicBehavior_[2] = true;
  inverseKinematicBehavior_[3] = false;
}

KraftHighLevelController::~KraftHighLevelController()
{
}

void KraftHighLevelController::init() {

  // select joystick
  model_.getState().setKraftJoystick(model_.getState().JoystickEnum::JOYSTICKXBOX360);

  // Initialize excavator model
  model_.initializeForController(timeStep_);

  // configure controllers
  inverseKinematic_ = new ControllerRos<kraft_highlevel_controller::InverseKinematic>(model_.getState(), model_.getCommand());
  inverseKinematic_->setParameterPath(ros::package::getPath("kraft_highlevel_controller"));
  inverseKinematic_->initialize(timeStep_);

  // init ros communication
  initializeMessages();
  initializeServices();
  initializePublishers();
  initializeSubscribers();
}

void KraftHighLevelController::cleanup() {}

void KraftHighLevelController::initializeMessages() {
  // init actuator commands
  {
    kraftActuatorCommands_.reset(new kraft_msgs::KraftActuatorCommands);
    kraft_description::initializeActuatorCommandsForKraft(*kraftActuatorCommands_);
  }
}

void KraftHighLevelController::initializeServices() {
}

void KraftHighLevelController::initializePublishers() {
  kraftActuatorCommandsPublisher_ = advertise<kraft_msgs::KraftActuatorCommands>("kraft_actuator_commands","/kraft_actuator_commands", 1);
  gripperBasePub_ = advertise<kraft_msgs::GripperBasePose>("gripper_base_pose", "/gripper_base_pose", 1);
}

void KraftHighLevelController::initializeSubscribers() {
  joystickSubscriber_ = subscribe("kraft_joystick", "/kraft_joystick", 1, &KraftHighLevelController::kraftJoystickCallback, ros::TransportHints().tcpNoDelay());
  kraftStateSubscriber_ = subscribe("kraft_state", "/kraft_state", 1, &KraftHighLevelController::kraftStateCallback, ros::TransportHints().tcpNoDelay());
  gripperBaseReferenceSub_ = subscribe("gripper_base_reference", "/gripper_base_reference", 100, &KraftHighLevelController::GripperBaseReferenceCallback);
}

void KraftHighLevelController::publish()  {
  // if there are subscribers publish actuator commands
  if(kraftActuatorCommandsPublisher_.getNumSubscribers() > 0u) {
    std::lock_guard<std::mutex> lock(mutexKraftActuatorCommands_);
    {
      model_.getKraftActuatorCommands(kraftActuatorCommands_);
      kraft_msgs::KraftActuatorCommandsConstPtr kraftActuatorCommands(new kraft_msgs::KraftActuatorCommands (*kraftActuatorCommands_));
      kraftActuatorCommandsPublisher_.publish(kraftActuatorCommands_);
    }
  }
}

void KraftHighLevelController::kraftStateCallback(const kraft_msgs::KraftState::ConstPtr& msg) {
  updateKraftControllerAndPublish(msg);
}

void KraftHighLevelController::GripperBaseReferenceCallback(const kraft_msgs::GripperBaseReference::ConstPtr& msg){

  tf::quaternionMsgToTF(msg->GripperBaseReference_pose.orientation , w_q_EEref_);

  w_r_EEref_(0) = msg->GripperBaseReference_pose.position.x;
  w_r_EEref_(1) = msg->GripperBaseReference_pose.position.y;
  w_r_EEref_(2) = msg->GripperBaseReference_pose.position.z;

//  std::cout << " gripperBaseReferencePos = " << w_r_EEref_ << std::endl;

  inverseKinematicBehavior_[0] = true;
  inverseKinematicBehavior_[1] = msg->moveKraftPosition;
  inverseKinematicBehavior_[2] = msg->moveKraftOrientation;
  inverseKinematicBehavior_[3] = msg->stayVertical;

}

void KraftHighLevelController::updateKraftControllerAndPublish(const kraft_msgs::KraftState::ConstPtr& kraftState) {
  model_.setKraftState(kraftState);
  // set all commands to none
  for(auto& command:model_.getCommand().getActuatorCommands()) {
    command.setMode(kraft_model::Command::Mode::MODE_NONE);
  }
  // update Inverse Kinematic

  if(useInverseKinematic_)
  {
    inverseKinematic_->getGripperBaseState(w_r_EE_, w_q_EE_);
    inverseKinematic_->setGripperBaseReferenceState(w_r_EEref_, w_q_EEref_,inverseKinematicBehavior_);
    inverseKinematic_->advance(timeStep_);
  }

  // feed-through of sequence number
  kraftActuatorCommands_->seq = kraftState->seq;

  // publish actuator commands
  publish();

  // publish GripperBaseState

  auto gripperBase = boost::make_shared<kraft_msgs::GripperBasePose>();
  auto pose = boost::make_shared<geometry_msgs::Pose>();

  pose->position.x = w_r_EE_(0);
  pose->position.y = w_r_EE_(1);
  pose->position.z = w_r_EE_(2);

  tf::quaternionTFToMsg(w_q_EE_, pose->orientation);

  gripperBase->GripperBase_pose = *(pose);
  gripperBase->header.stamp = ros::Time::now();

  gripperBasePub_.publish(gripperBase);
}

void KraftHighLevelController::kraftJoystickCallback(const sensor_msgs::Joy::ConstPtr& msg) {
    // set command
  model_.setKraftJoystickCommands(msg);
  }

} /* namespace kraft_highlevel_controller */
