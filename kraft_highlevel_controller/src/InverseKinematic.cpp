/*
 * Copyright (c) 2015, Gabriel Hottiger
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Autonomous Systems Lab, ETH Zurich nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL  Gabriel Hottiger BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/*
 * ManualAxisController.cpp
 *
 *  Created on: Jun 12, 2015
 *      Author: Gabriel Hottiger
 */

// manual axis controller
#include "kraft_highlevel_controller/InverseKinematic.hpp"

// description
#include "kraft_description/kraft_names.hpp"
#include "kraft_description/enums/enums.hpp"


using namespace kraft_description;

namespace kraft_highlevel_controller{

InverseKinematic::InverseKinematic():
    Base("InverseKinematic"),
    gripperBaseReferenceInput_(false),
    moveKraftPosition_(true),
    moveKraftOrientation_(true),
    stayVertical_(false)
{
}

InverseKinematic::~InverseKinematic() {
}

bool InverseKinematic::create(double dt) {
  return true;
}


  // Used to publish calculated GripperBase from the model in the highlevel controller
void InverseKinematic::getGripperBaseState(Eigen::Vector3d& gripperBasePosHL, tf::Quaternion& gripperBaseQuatHL) {
	gripperBasePosHL = w_r_EE;
	gripperBaseQuatHL.setX(w_q_EE.x());
  gripperBaseQuatHL.setY(w_q_EE.y());
  gripperBaseQuatHL.setZ(w_q_EE.z());
  gripperBaseQuatHL.setW(w_q_EE.w());
}


  // Reference position and orientation for the Gripper in a Eigen Vector and a tf Quaternion
void InverseKinematic::setGripperBaseReferenceState(Eigen::Vector3d& gripperBaseReferencePosHL, tf::Quaternion& gripperBaseReferenceQuattf_, bool* inverseKinematicBehavior) {

  gripperBaseReferenceInput_ = inverseKinematicBehavior[0];
  moveKraftPosition_ = inverseKinematicBehavior[1];
  moveKraftOrientation_ = inverseKinematicBehavior[2];
  stayVertical_ = inverseKinematicBehavior[3];

  w_r_ref = gripperBaseReferencePosHL;
  if(stayVertical_){
  w_q_ref.x() = 0;
  w_q_ref.y() = 0;
  w_q_ref.z() = 0;
  w_q_ref.w() = 1;
  }
  else{
  w_q_ref.x() = gripperBaseReferenceQuattf_.getX();
  w_q_ref.y() = gripperBaseReferenceQuattf_.getY();
  w_q_ref.z() = gripperBaseReferenceQuattf_.getZ();
  w_q_ref.w() = gripperBaseReferenceQuattf_.getW();
  }

}


Eigen::Matrix<double,3,3> InverseKinematic::skewMatrix(Eigen::Vector3d x){
  // Formula for the SkewMatrix from the second robot dynamics exercise

  Eigen::Matrix<double,3,3> Skew;

  Skew << 0,-x(2), x(1),
          x(2),0,-x(0),
          -x(1),x(0),0;

  return Skew;
}


  // Calculate xiMatrix for the calculation of J_rot_quat
Eigen::Matrix<double,4,3> InverseKinematic::xiMatrix(){

  Eigen::Matrix<double,4,3> xi;
  Eigen::Quaterniond q_Eigen(w_EulerRotMat);
  Eigen::Vector4d q_Vector;

  // fill qVector with the values of the quaternion WXYZ
  q_Vector << q_Eigen.w(), q_Eigen.x(), q_Eigen.y(), q_Eigen.z();
  Eigen::Vector3d q_VectorXYZ;

  // take only XYZ values for the calculations below
  q_VectorXYZ = q_Vector.block<3,1>(1,0);

  // calculate xiMatrix with the formula from the second robot dynamics exercise
  Eigen::Matrix<double,3,3> skewMatrix_q_VectorW;
  skewMatrix_q_VectorW = q_Vector(0)*Eigen::Matrix<double, 3, 3>::Identity() + skewMatrix(q_VectorXYZ);

  xi << -q_VectorXYZ.transpose(),
      skewMatrix_q_VectorW;

  return 0.5*xi;
}

bool InverseKinematic::advance(double dt) {

  // Proportional factor multiplied with the velocity vector filled with w and v
  int factor = 10;

	//Initialize variables for velocity calculation
	Eigen::Vector3d v(0,0,0);
	Eigen::Vector4d w(0,0,0,0);

  //Get the actual Pos w_r_EE and the actual orientation EEEuler of the Kraft_Gripper_Base
	w_EulerRotMat = getState().getKraftModelPtr()->getOrientationWorldToBody(kraft_description::KraftBodyEnum::Kraft_Gripper_Base);
	w_q_EE = Eigen::Quaterniond(w_EulerRotMat);
	w_q_EE = w_q_EE.inverse();
	w_q_EE.normalize();
	getState().getKraftModelPtr()->getPositionWorldToBody(w_r_EE, kraft_description::KraftBodyEnum::Kraft_Gripper_Base,kraft_description::KraftCoordinateFrame::WORLD);

	// Set reference position equal endeffektor position if no reference input comes
	if (!gripperBaseReferenceInput_)
	{
//	  std::cout << "inside gripperBaseReferenceInput_ if statement" << std::endl;
	  w_r_ref = w_r_EE;
	  w_q_ref = w_q_EE;
	 }

	// Calculate velocity
    v = w_r_ref - w_r_EE;
//    std::cout << "w_r_EE" << w_r_EE << std::endl;
//    std::cout << "reference Position" << w_r_ref << std::endl;

  // calculate the difference in orientation and save it in w
  Eigen::Quaterniond Diff;

  // Just normal component wise difference
  Diff.x() = w_q_ref.x() - w_q_EE.x();
  Diff.y() = w_q_ref.y() - w_q_EE.y();
  Diff.z() = w_q_ref.z() - w_q_EE.z();
  Diff.w() = w_q_ref.w() - w_q_EE.w();

  // Save the difference in w
  w(0) = Diff.w();
  w(1) = Diff.x();
  w(2) = Diff.y();
  w(3) = Diff.z();

//std::cout << "moveKraftPosition" << moveKraftPosition_<< std::endl;
  // Set v or w to zero if the respective bool is false
  if (!moveKraftPosition_)
  {
    v << 0,0,0;
   }
  if (!moveKraftOrientation_)
  {
    w << 0,0,0,0;
   }

//  std::cout << "GripperBaseReferenceQuaternion = " << std::endl;
//  std::cout << w_q_ref.x() << std::endl;
//  std::cout << w_q_ref.y() << std::endl;
//  std::cout << w_q_ref.z() << std::endl;
//  std::cout << w_q_ref.w() << std::endl;
//
//	std::cout << "GripperBaseQuaternion from Model w_q_EE = " << std::endl;
//	std::cout << w_q_EE.x() << std::endl;
//  std::cout << w_q_EE.y() << std::endl;
//  std::cout << w_q_EE.z() << std::endl;
//  std::cout << w_q_EE.w() << std::endl;

	//Jacobian
	Eigen::MatrixXd jacobian = Eigen::MatrixXd::Zero(6, kraft_model::noGenCoordWholeBody);
	getState().getKraftModelPtr()->getJacobianSpatialWorldToBody(jacobian,KraftBranchEnum::KRAFT_ARM,KraftBodyNodeEnum::Kraft_Gripper_Base,KraftCoordinateFrame::WORLD);
	Eigen::MatrixXd jacobianBaseToGripperBase = jacobian.block<6,6>(0,static_cast<int>(GeneralizedCoordinatesEulerXyzEnum::Kraft_Base_to_Base_Plate));

	//Create Vector vel = [w;v]
	Eigen::Matrix<double, 7, 1> vel;
	vel << 	w,
			v;

//	std::cout << "vel following:" << std::endl;
//	std::cout << "vel: " << std::endl << vel << std::endl << "vel norm: " << vel.norm() << std::endl;
//	std::cout << "vel ending" << std::endl;

	//Create gain for vel
	vel = factor*vel;

//	for (int i = 0; i < vel.size(); i++)
//	{
//	  if(vel(i) > 0.5)
//	  vel(i) = 0.5;
//	}

	//Implement code from the robot dynamics exercise

	// tf::Quaternions w_q_ref and w_q_EE

	Eigen::MatrixXd jacobianRotation = jacobianBaseToGripperBase.block<3,6>(0,0);
  Eigen::MatrixXd jacobianPosition = jacobianBaseToGripperBase.block<3,6>(3,0);

	Eigen::MatrixXd jacobianRotationQuat;

	jacobianRotationQuat = xiMatrix() * w_EulerRotMat.transpose() * jacobianRotation;

	Eigen::MatrixXd jacobianSpatialQuat = Eigen::MatrixXd::Zero(7,6);

//	std::cout << "jacobianRotationQuat size = " << jacobianRotationQuat.rows() << jacobianRotationQuat.cols() << std::endl;
//  std::cout << "jacobianSpatialQuat size = " << jacobianSpatialQuat.rows() << jacobianSpatialQuat.cols() << std::endl;

	jacobianSpatialQuat.block<4,6>(0,0) = jacobianRotationQuat;
	jacobianSpatialQuat.block<3,6>(4,0) = jacobianPosition;

	Eigen::MatrixXd jacobianSpatialQuatInverted;
	pseudoInverse(jacobianSpatialQuat, jacobianSpatialQuatInverted);

	//Create Vector q_p with joint velocities
	Eigen::VectorXd q_p;
	q_p = 0.7*jacobianSpatialQuatInverted*vel;
//	std::cout << "q_p following:" << std::endl;
//	std::cout << q_p << std::endl;
//	std::cout << "q_p ending" << std::endl;

	for(int i = 0; i<static_cast<int>(kraft_description::KraftActuatorEnum::NrActuators); i++) {
		getCommand().getActuatorCommands().at(i).setJointVelocity(q_p(i));
		getCommand().getActuatorCommands().at(i).setMode(Command::Mode::MODE_JOINTVELOCITY);
	}

  //Create Vector Teta_p with joint positions

//  Eigen::VectorXd Teta(6);
//
//    for(int i = 0; i < Teta.size(); i++){
//        Teta(i) = 1;
//    }
//
//    //  for(int i = 0; i<static_cast<int>(kraft_description::KraftActuatorEnum::NrActuators); i++) {
//    //    getCommand().getActuatorCommands().at(i).setJointPosition(Teta(i));
//    //    getCommand().getActuatorCommands().at(i).setMode(Command::Mode::MODE_JOINTPOSITION);
//    //  }
//
////    std::cout << "Teta following:" << std::endl;
////    std::cout << Teta << std::endl;
////    std::cout << "Teta ending" << std::endl;

	return true;
}

bool InverseKinematic::reset(double dt) {
  return initialize(dt);
}

bool InverseKinematic::change() {
  return true;
}

bool InverseKinematic::cleanup() {
  return true;
}

void InverseKinematic::setParameterPath(std::string path) {
}

bool InverseKinematic::loadParameters() {
  return true;
}

} // kraft_highlevel_controller
