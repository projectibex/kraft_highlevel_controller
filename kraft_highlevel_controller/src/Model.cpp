/*
 * Copyright (c) 2014, Christian Gehring
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Autonomous Systems Lab, ETH Zurich nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL  Christian Gehring BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*!
 * @file    Model.cpp
 * @author  Preisig Jan, Katz Michael
 * @date    Mai, 2016
 */

// highlevel-controller
#include "kraft_highlevel_controller/Model.hpp"

// description
#include "kraft_description/kraft_names.hpp"
#include "kraft_description/kraft_state.hpp"

// actuator
#include "kraft_actuator/ConvertRosMessages.hpp"

// kraft model
#include "kraft_model/kraft/kraft.hpp"
#include "kraft_model/typedefs.hpp"


// ros
#include <ros/package.h>

namespace model {

Model::Model():
      updateStamp_(0.0),
      kraftModel_(),
      kraftstate_(),
      kraftcommand_()
{
}

Model::~Model()
{

}

kraft_model::KraftModel* Model::getModel() {
  return kraftModel_.get();
}

kraft_model::State& Model::getState() {
  return kraftstate_;
}

kraft_model::Command& Model::getCommand() {
  return kraftcommand_;
}

const kraft_model::State& Model::getState() const {
  return kraftstate_;
}

const kraft_model::Command& Model::getCommand() const {
  return kraftcommand_;
}

void Model::initializeForController(double dt) {
  /* load kraft model from URDF */
  kraftModel_.reset(new kraft_model::KraftModel(dt));
  std::string path = ros::package::getPath("kraft_description")+"/urdf/kraft.urdf";
  kraftModel_->initModelFromUrdfFile(path.c_str(), false);

  /* init kraft model */
  kraftModel_->initialize();

  /* init state and command */
  kraftstate_.setKraftModelPtr(kraftModel_.get());
  kraft_model::initializeStateForKraft(kraftstate_);
  kraft_model::initializeCommandForKraft(kraftcommand_, *kraftModel_);

}

void Model::reinitialize(double dt) {
  kraftModel_->initialize();
}

void Model::setKraftState(const kraft_msgs::KraftState::ConstPtr& kraftStatemsg) {
//  std::cout << "setKraftState started!!!!!!!!!!" << std::endl;

  /* kraft state*/
  kraft_model::KraftState kraftstate;
  /* static joint states*/
  static kraft_model::JointTorques jointTorques;
  static kraft_model::JointPositions jointPositions;
  static kraft_model::JointVelocities jointVelocities;
  /* set joint states*/
  for (int i = 0; i < jointPositions.toImplementation().size(); i++) {
    jointTorques(i) = kraftStatemsg->joints.effort[i];
    jointPositions(i) = kraftStatemsg->joints.position[i];
    jointVelocities(i) = kraftStatemsg->joints.velocity[i];
  }

  kraftstate.setJointPositions(jointPositions);
//  std::cout << "setJointPositions set!!!!!!!!!!" << std::endl;
  kraftstate.setJointVelocities(jointVelocities);
//  std::cout << "setJointVelocities set!!!!!!!!!!" << std::endl;
  kraftModel_->setJointTorques(jointTorques);
//  std::cout << "setJointTorques set!!!!!!!!!!" << std::endl;

  /* set BasePlate pose*/
  kraftstate.setPositionWorldToBaseInWorldFrame(kraft_model::Position( kraftStatemsg->BasePlate_pose.position.x,
                                                                      kraftStatemsg->BasePlate_pose.position.y,
                                                                      kraftStatemsg->BasePlate_pose.position.z));
//  std::cout << "set BasePlate pose!!!!!!!!!!" << std::endl;
  kraft_model::RotationQuaternion orientationWorldToBase(kraftStatemsg->BasePlate_pose.orientation.w,
                                                             kraftStatemsg->BasePlate_pose.orientation.x,
                                                             kraftStatemsg->BasePlate_pose.orientation.y,
                                                             kraftStatemsg->BasePlate_pose.orientation.z);
  kraftstate.setOrientationWorldToBase(orientationWorldToBase);
//  std::cout << "set BasePlate pose!!!!!!!!!!" << std::endl;
  /* update kraft model and state*/
  kraftModel_->setState(kraftstate, true, false, false);
//  std::cout << "setState executed!!!!!!!!!!" << std::endl;
  kraftstate_.copyStateFromKraftModel();
  kraftstate_.setStatus((kraft_model::State::StateStatus)kraftStatemsg->state);



}

void Model::initializeKraftState(kraft_msgs::KraftState& kraftRobotState) const {
  kraft_description::initializeManipulatorStateForKraft(kraftRobotState);
}

void Model::initializeKraftJointState(sensor_msgs::JointState& kraftJointState) const {
  kraft_description::initializeJointStateForKraft(kraftJointState);
}

void Model::getKraftActuatorCommands(kraft_msgs::KraftActuatorCommandsPtr& kraftActuatorCommandsMsg) {
  /* get current ros time for stamps */
  ros::Time stamp = ros::Time::now();

  /* convert actuator references to cylinder commands */
  kraft_model::ActuatorCommandRobotContainer kraftActuatorCommands = getCommand().getActuatorCommands();

  /* convert commands to ros msgs */
  for (int i=0; i< kraftActuatorCommandsMsg->commands.size(); i++) {
    kraftActuatorCommandsMsg->commands[i].header.stamp = stamp;
    kraft_actuator::ConvertRosMessages::writeToMessage(kraftActuatorCommandsMsg->commands[i], kraftActuatorCommands.at(i));
  }
}


void Model::setKraftJoystickCommands(const sensor_msgs::Joy::ConstPtr& msg) {
  /* get joystick*/
  kraft_utils::KraftJoystickInterface* joystick = kraftstate_.getKraftJoystickPtr();

  /* set axes*/
  for (int i=0; i<msg->axes.size();i++) {
    joystick->setAxis(i, msg->axes[i]);
  }

  /* set buttons*/
  for (int i=0; i<msg->buttons.size();i++) {
    joystick->setButton(i, msg->buttons[i]);
  }

  /* map joystick axes, buttons to Kraft joystick commands */
  joystick->setKraftAxesAndButtonsFromJoystick();
}

} /* namespace model */
