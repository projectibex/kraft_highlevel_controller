// ros
#include "ros/ros.h"
#include "roscpp_nodewrap/NodeImpl.h"
#include <ros/package.h>
#include <ros/callback_queue.h>

// standart Message
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Pose.h>
#include "sensor_msgs/JointState.h"

// kraft msgs
#include "kraft_msgs/KraftActuatorCommands.h"
#include "kraft_msgs/KraftState.h"
#include "kraft_msgs/GripperState.h"
#include "kraft_msgs/GripperCommand.h"
#include "kraft_msgs/StonesPose.h"
#include "kraft_msgs/ShovelPose.h"
#include "kraft_msgs/GripperBaseReference.h"
#include "kraft_msgs/GripperBasePose.h"

// m545 msgs
#include "m545_msgs/M545State.h"
#include "m545_msgs/M545ActuatorCommands.h"

// joystick interfaces
#include <ManualControllerJoystick/ManualControllerJoystickInterface.hpp>
#include <ManualControllerJoystick/ManualControllerJoystickXBOX360.hpp>
#include <ManualControllerJoystick/ManualControllerJoystick3DConnexion.hpp>

// yaml
#include <yaml-cpp/yaml.h>

// eigen
#include <Eigen/Core>
#include <tf/transform_datatypes.h>
#include <Eigen/Geometry>

// tf
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>


namespace kraft_controller{

class KraftController: public nodewrap::NodeImpl{
 public:
  KraftController();
  virtual ~KraftController();

 protected:
  virtual void init();
  virtual void cleanup();

  void initializeSubscribers();
  void initializePublisher();
  void initializeService();


  void GripperBaseCallback(const kraft_msgs::GripperBasePose::ConstPtr& msg);
  void m545ShovelCallback(const kraft_msgs::ShovelPose::ConstPtr& shovelmsg);
  void kraftStonesCallback(const kraft_msgs::StonesPose::ConstPtr& stonesmsg);

  void m545StateCallback(const m545_msgs::M545State::ConstPtr& statemsg);
  void m545CommandsPublisher();

  void JoystickCallback(const sensor_msgs::Joy::ConstPtr& joymsg);
  void setJoystick();

  void publishGripperBaseForManualControl();
  void publish(tf::Quaternion& gripperBaseQuatPublish, Eigen::Vector3d& gripperBasePosPublish, bool moveKraftPosition, bool moveKraftOrientation, bool stayVertical);

  void autonomousControl();
  bool moveToPosition(Eigen::Vector3d& position);
  bool changeToOrientaion(int whichStone);

  void kraftGripperStateCallback(const kraft_msgs::GripperState::ConstPtr& msg);
  bool gripperCommandPublisher(std::string gripperDesiredState);

 private:

  ros::Subscriber joySub_;
  ros::Subscriber gripperBaseSub_;
  ros::Subscriber shovelSub_;
  ros::Subscriber m545StatesSub_;
  ros::Subscriber stonesPoseSub_;
  ros::Publisher  m545CommandsPub_;
  ros::Publisher  gripperBaseReferencePub_;


  kraft_utils_manual_controller::ManualControllerJoystickInterface* joystick_;

  // Quaternion and Position Vector to publish to the GripperReference Msg
  tf::Quaternion  w_q_EE_publish_;
  Eigen::Vector3d w_r_EE_publish_;

  // point and orientation moved by joystick
  Eigen::Vector3d w_r_EE_ref_;
  tf::Quaternion  w_q_EE_ref_;
  Eigen::Vector3d w_euler_EE_ref_;

  // point and orientation of the EndEffector received by the KraftHighLevelController
  tf::Quaternion  w_q_EE_;
  Eigen::Vector3d w_euler_EE_;
  Eigen::Vector3d w_r_EE_;

  // Position of the shovel
  Eigen::Vector3d shovelPos_;

  // bools for Inverse Kinematic behavior
  bool moveKraftPosition_;
  bool moveKraftOrientation_;
  bool stayVertical_;

  // bool to stop joystick inputs when autonous control is running
  bool running_;

  // bool to set the Reference of the joystick control once in the beginning
  bool setGripperBaseReference_;

  // bool to start manual control after initializing and stop listening to fixed command
  bool startManualControl_;

  //  check whether excavator is in the right position
  bool excavatorInRightPosition_;

  // check for gripper closed or open
  bool gripperOpen_;

  // bool to stop autonomous at any time with left button
  bool stopAutonomous_;


  /////////////////////////////////////////////////////////////////////////////
  ////////////////// for autonomous control ///////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////

  // Variables for the m545Commandspublisher to seth the shovel to the ground
  // state reference position because state is not equal to the command -> weird I know
  float* kraftJointPosition = new float[23];
  bool* m545JointPositionsReached = new bool[4];
  float m545JointReferencePosition[4] = {0.1, 0.0, 0.000739, -0.57316};
  float m545JointReferencePositionState[4] = {0.1, 0.555, 0.000739, -0.57316};

  int NoStones;
  double r_Gripper_to_EE_;
  Eigen::Vector3d* stonesPosition = new Eigen::Vector3d;
  tf::Quaternion* stonesQuat = new tf::Quaternion;
  Eigen::Vector3d* stonesEuler = new Eigen::Vector3d;

  // Waypoints for manual control
  Eigen::Vector3d stone_Wp1_AS_;
  Eigen::Vector3d* stone_Wp2_ = new Eigen::Vector3d;
  Eigen::Vector3d* stone_Wp3_ = new Eigen::Vector3d;
  Eigen::Vector3d* stone_Wp4_ = new Eigen::Vector3d;
  Eigen::Vector3d* stone_Wp5_ = new Eigen::Vector3d;


  //////////////////////////////////////////////////////////////////////////////
  ////////////////////////// for Gripper control ///////////////////////////////
  //////////////////////////////////////////////////////////////////////////////

  ros::Publisher gripperActuatorPub_;
  ros::Subscriber kraftStateSub_;

  kraft_msgs::GripperCommand gripperCommandmsg_;

  float gripperCylinderPos_;
  float gripperCylinderVel_;
  float gripperCylinderForce_;

  };
};
