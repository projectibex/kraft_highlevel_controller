#include "../include/kraft_controller/KraftController.hpp"

namespace kraft_controller{


KraftController::KraftController():
        joystick_(nullptr),
        running_(false),
        setGripperBaseReference_(true),
        excavatorInRightPosition_(false),
        gripperOpen_(false),
        moveKraftPosition_(true),
        moveKraftOrientation_(true),
        stayVertical_(false),
        stopAutonomous_(false),
        startManualControl_(false),
        gripperCylinderPos_(0.0),
        gripperCylinderVel_(0.0),
        gripperCylinderForce_(0.0),
        r_Gripper_to_EE_(0.2)
{
    NoStones = 20;

    stonesPosition = new Eigen::Vector3d[NoStones];
    stonesQuat = new tf::Quaternion[NoStones];
    stonesEuler = new Eigen::Vector3d[NoStones];

    stone_Wp2_ = new Eigen::Vector3d[NoStones];
    stone_Wp3_ = new Eigen::Vector3d[NoStones];
    stone_Wp4_ = new Eigen::Vector3d[NoStones];
    stone_Wp5_ = new Eigen::Vector3d[NoStones];
}

KraftController::~KraftController(){}

void KraftController::init(){
  initializeSubscribers();
  initializePublisher();
  setJoystick();
}

void KraftController::cleanup() {}

void KraftController::initializeSubscribers(){
  joySub_         = subscribe("kraft_joystick", "/kraft_joystick", 1, &KraftController::JoystickCallback);
  gripperBaseSub_ = subscribe("gripper_base_pose", "/gripper_base_pose", 1, &KraftController::GripperBaseCallback);
  m545StatesSub_  = subscribe("m545_state", "/m545_state", 1, &KraftController::m545StateCallback);
  stonesPoseSub_  = subscribe("kraft_stones_poses", "/kraft_stones_poses", 1, &KraftController::kraftStonesCallback);
  shovelSub_      = subscribe("kraft_shovel_pose", "/kraft_shovel_pose", 1, &KraftController::m545ShovelCallback);
  kraftStateSub_ = subscribe("gripper_state","/gripper_state",1,&KraftController::kraftGripperStateCallback);
}

void KraftController::initializePublisher(){
  gripperBaseReferencePub_  = advertise<kraft_msgs::GripperBaseReference>("gripper_base_reference", "/gripper_base_reference", 100);
  m545CommandsPub_ = advertise<m545_msgs::M545ActuatorCommands>("m545_actuator_commands", "/m545_actuator_commands", 100);
  gripperActuatorPub_ = advertise<kraft_msgs::GripperCommand>("kraft_gripper_commands", "/kraft_gripper_command",1);

}

void KraftController::initializeService(){
}


// Set either 3DConnexion or JoystickXBOX Joysticks in the default_params file
void KraftController::setJoystick(){

  std::string packagepath = ros::package::getPath("kraft_manual_controller");
  YAML::Node default_params = YAML::LoadFile(packagepath + "/param/default_params.yaml");

  int selectedJoystick = default_params["joystick"].as<int>();

  switch(selectedJoystick){
    case 0:
      joystick_ = new kraft_utils_manual_controller::JoystickXBOX360();
      break;
    case 1:
      joystick_ = new kraft_utils_manual_controller::Joystick3DConnexion();
  }
} // setJoystick


// Call back for the state of the EE calculated from the HighlevelController. Saved in Quaternion w_q_EE_ and Position w_r_EE_.
void KraftController::GripperBaseCallback(const kraft_msgs::GripperBasePose::ConstPtr& msg){

  tf::quaternionMsgToTF(msg->GripperBase_pose.orientation , w_q_EE_);

  w_r_EE_ << msg->GripperBase_pose.position.x,msg->GripperBase_pose.position.y,msg->GripperBase_pose.position.z;

  double yaw,pitch,roll;
  tf::Matrix3x3(w_q_EE_).getEulerYPR(yaw,pitch,roll,1);
  w_euler_EE_ << roll,pitch,yaw;
} // GripperBaseCallback


// Callback for the joystick input. Move reference position and orientation around with joystick increments
void KraftController::JoystickCallback(const sensor_msgs::Joy::ConstPtr& joymsg){

  // write Joystick axes and buttons
  for(int i=0; i<joymsg->axes.size();i++){
    joystick_->setAxis(i,joymsg->axes[i]);
  }
  for(int i=0; i<joymsg->buttons.size();i++){
    joystick_->setButton(i,joymsg->buttons[i]);
  }
  joystick_->setKraftAxesAndButtonsFromJoystick();

  // if Joystick button left is pressed        -> start manual control and start motion of the manipulator
  //                  if pressed a second time ->
  //                  if pressed a third time  -> start autonomous control
  if(joystick_->getJoyButton(kraft_utils_manual_controller::ManualControllerJoystickInterface::Buttons::ENABLE))
  {
    if(!startManualControl_)
    {
      startManualControl_ = true;
    }
    else{
      if(!excavatorInRightPosition_){
        m545CommandsPublisher();
      }
      else if(!running_){
        stopAutonomous_ = false;
        autonomousControl();
      }
      else{
        std::cout << "autonomousControl running!!" << std::endl;
      }
    }
  }

  // If Joystick button right is pressed and if autonomous is not running  -> Gripper open and close
  //                                         if autonoumous is running     -> stop autonomous control
  if(joystick_->getJoyButton(kraft_utils_manual_controller::ManualControllerJoystickInterface::Buttons::MODESWITCH))
  {
    if(!running_)
    {
      if(!gripperOpen_)
      {
        gripperCommandPublisher("open");
        gripperOpen_ = true;
      }
      else
      {
        gripperCommandPublisher("close");
        gripperOpen_ = false;
      }
    }
    else
    {
      stopAutonomous_ = true;
    }
  }

  if(!running_){
    publishGripperBaseForManualControl();
  }
} // JoystickCallback

void KraftController::publishGripperBaseForManualControl(){

  auto gripperBaseReference = boost::make_shared<kraft_msgs::GripperBaseReference>();
  auto pose = boost::make_shared<geometry_msgs::Pose>();

  float factorPosition    = 0.005;
  float factorOrientation = 0.01;

  // made because starting in zero position of the Kraft lead to uncontrolled movement of the manipulator
  // poisition hardcoded as a reasonable position for all joint
  // TODO: make this starting posiition dependent on starting position
  if (startManualControl_)
  {
    w_r_EE_ref_(0) +=  factorPosition  * joystick_->getX();
    w_r_EE_ref_(1) +=  factorPosition  * joystick_->getY();
    w_r_EE_ref_(2) +=  factorPosition  * joystick_->getZ();
  }
  else
  {
    w_r_EE_ref_(0) = 8.3;
    w_r_EE_ref_(1) = 12.5;
    w_r_EE_ref_(2) = -8.5;
  }

  // include only yaw rotation to pick up the stones. Other rotation are working but are not usefull at the moment
  // starting orientation is set to vertical and only yaw orientation can be controlled
  if(startManualControl_)
  {
    double yaw, pitch, roll;
    tf::Matrix3x3(w_q_EE_ref_).getEulerYPR(yaw, pitch, roll);

    yaw += factorOrientation*joystick_->getYaw();
//         roll += factorOrientation*joystick_->getRoll();
//         pitch += factorOrientation*joystick_->getPitch();

    w_q_EE_ref_.setRPY(roll,pitch,yaw);
  }
  else
  {
    w_q_EE_ref_.setW(1);
    w_q_EE_ref_.setX(0);
    w_q_EE_ref_.setY(0);
    w_q_EE_ref_.setZ(0);
  }

  publish(w_q_EE_ref_, w_r_EE_ref_, moveKraftPosition_, moveKraftOrientation_, stayVertical_);
} // publishGripperBaseForManualControl

 // publish the passed quaternion and the position in addition to the 3 bools for inverse kinematic behavior
void KraftController::publish(tf::Quaternion& w_q_EE_publish, Eigen::Vector3d& w_r_EE_publish, bool moveKraftPosition, bool moveKraftOrientation, bool stayVertical){

  auto gripperBasePublish = boost::make_shared<kraft_msgs::GripperBaseReference>();
  auto pose = boost::make_shared<geometry_msgs::Pose>();

  pose->position.x = w_r_EE_publish(0);
  pose->position.y = w_r_EE_publish(1);
  pose->position.z = w_r_EE_publish(2);

  tf::quaternionTFToMsg(w_q_EE_publish, pose->orientation);
  gripperBasePublish->GripperBaseReference_pose = *(pose);
  gripperBasePublish->moveKraftPosition = moveKraftPosition;
  gripperBasePublish->moveKraftOrientation = moveKraftOrientation;
  gripperBasePublish->stayVertical = stayVertical;
  gripperBasePublish->header.stamp = ros::Time::now();

  gripperBaseReferencePub_.publish(gripperBasePublish);
} // publish


 ///////////////////////////////////////////////////////////////////////
 //////////////////////  M545Controller ////////////////////////////////
 ///////////////////////////////////////////////////////////////////////

void KraftController::m545ShovelCallback(const kraft_msgs::ShovelPose::ConstPtr& shovelmsg){
  shovelPos_(0) = shovelmsg->Shovel_pose.position.x;
  shovelPos_(1) = shovelmsg->Shovel_pose.position.y;
  shovelPos_(2) = shovelmsg->Shovel_pose.position.z;
  // set first waypoint for all stones above the shovel
  stone_Wp1_AS_(0) = shovelPos_(0)+0.4;
  stone_Wp1_AS_(1) = shovelPos_(1);
  stone_Wp1_AS_(2) = shovelPos_(2)+0.5;
} // m545ShovelCallback

 void KraftController::m545StateCallback(const m545_msgs::M545State::ConstPtr& statemsg){
   for (int i = 0; i < 23; i++){
     kraftJointPosition[i] = statemsg->joints.position[i];
   }
 } // m545StateCallback

 // Move shovel to the ground and don't move kraft until the ground is reached.
 // Set the position to a reasonable position from where it can be manually controlled or autonomous control can be started.
 void KraftController::m545CommandsPublisher(){

   std::cout << "started control" << std::endl;

   auto m545ActuatorCommands = boost::make_shared<m545_msgs::M545ActuatorCommands>();

   m545JointPositionsReached[0] = false;
   m545JointPositionsReached[1] = false;
   m545JointPositionsReached[2] = false;
   m545JointPositionsReached[3] = false;

   float factor = 1.1;
   bool continue_ = true;
   bool change = true;
   bool stop = false;
   while(continue_)
   {
     moveKraftPosition_ = false;
     stayVertical_ = true;
     for (int i = 0; i < 23; i++)
     {
       m545ActuatorCommands->commands[i].mode = 0;
       m545ActuatorCommands->commands[i].referencePistonPosition = 0.0;
       m545ActuatorCommands->commands[i].referencePistonVelocity = 0.0;
       m545ActuatorCommands->commands[i].referenceCylinderForce = 0.0;
       m545ActuatorCommands->commands[i].referenceImpedance = 0.0;
       // uncomment the following to move the excavator few seconds back and to the starting position again to test that reference position is in world reference frame
//       if (change)
//       {
//       if(i == 3 || i == 7 || i == 11 || i == 16)
//       {
//         std::cout << "in first if statement" << std::endl;
//                  m545ActuatorCommands->commands[i].mode = 2;
//                  m545ActuatorCommands->commands[i].referencePistonPosition = 0.0;
//                  m545ActuatorCommands->commands[i].referencePistonVelocity = -2.0;
//                  m545ActuatorCommands->commands[i].referenceCylinderForce = 0.0;
//                  m545ActuatorCommands->commands[i].referenceImpedance = 0.0;
//       }
//       }
//
//       if (!change)
//       {
//         if(i == 3 || i == 7 || i == 11 || i == 16)
//         {
//           std::cout << "in second if statement" << std::endl;
//                    m545ActuatorCommands->commands[i].mode = 2;
//                    m545ActuatorCommands->commands[i].referencePistonPosition = 0.0;
//                    m545ActuatorCommands->commands[i].referencePistonVelocity = 2.0;
//                    m545ActuatorCommands->commands[i].referenceCylinderForce = 0.0;
//                    m545ActuatorCommands->commands[i].referenceImpedance = 0.0;
//                    std::cout << "stop set to true" << std::endl;
//                    stop = true;
//         }
//       }
       if (i == 18)
       {

         change = false;
         std::cout << "change = false" << std::endl;
       }
       if (i>18)
       {
         if ((kraftJointPosition[i] > m545JointReferencePositionState[i-19]-0.005) && (kraftJointPosition[i] < m545JointReferencePositionState[i-19]+0.005)){
           m545ActuatorCommands->commands[i].mode = 1;
           m545ActuatorCommands->commands[i].referencePistonPosition = m545JointReferencePosition[i-19];
           std::cout << "jointpositionREached = " << std::endl;
           std::cout << m545JointPositionsReached[0] << std::endl;
           std::cout << m545JointPositionsReached[1] << std::endl;
           std::cout << m545JointPositionsReached[2] << std::endl;
           std::cout << m545JointPositionsReached[3] << std::endl;
           m545JointPositionsReached[i-19] = true;
         }
         else{
           switch (i)
           {
             case 19:
               m545ActuatorCommands->commands[i].mode = 2;
               m545ActuatorCommands->commands[i].referencePistonVelocity = -0.1*factor;
               break;
             case 20:
               m545ActuatorCommands->commands[i].mode = 2;
               m545ActuatorCommands->commands[i].referencePistonVelocity = -0.2*factor;
               break;
             case 21:
               m545ActuatorCommands->commands[i].mode = 2;
               m545ActuatorCommands->commands[i].referencePistonVelocity = -0.4*factor;
               break;
             case 22:
               m545ActuatorCommands->commands[i].mode = 2;
               m545ActuatorCommands->commands[i].referencePistonVelocity = -0.2*factor;
               break;
           }
         }
       }
     }

     if(m545JointPositionsReached[0] && m545JointPositionsReached[1] && m545JointPositionsReached[2] && m545JointPositionsReached[3])
     {
       continue_ = false;
       moveKraftPosition_ = true;
       stayVertical_ = false;
     }
     moveKraftOrientation_ = true;
//     w_r_EE_ref_ << 8.3,12.5,-8.5;
     w_r_EE_ref_ << 9, 12.5,-9;
     w_q_EE_ref_.setW(1);
     w_q_EE_ref_.setX(0);
     w_q_EE_ref_.setY(0);
     w_q_EE_ref_.setZ(0);
     publish(w_q_EE_ref_, w_r_EE_ref_, moveKraftPosition_, moveKraftOrientation_, stayVertical_);
     m545CommandsPub_.publish(m545ActuatorCommands);
     ros::spinOnce();
     // part of the above mentioned function to move excavator back
//
//     sleep(4.5);
//     sleep(0.75);
//     if (stop)
//     {
//
//       for (int i = 0; i < 23; i++)
//       {
//         std::cout << "in stop if statement" << std::endl;
//         m545ActuatorCommands->commands[i].mode = 2;
//         m545ActuatorCommands->commands[i].referencePistonPosition = 0.0;
//         m545ActuatorCommands->commands[i].referencePistonVelocity = 0.0;
//         m545ActuatorCommands->commands[i].referenceCylinderForce = 0.0;
//         m545ActuatorCommands->commands[i].referenceImpedance = 0.0;
//       }
//       m545CommandsPub_.publish(m545ActuatorCommands);
//       break;
//     }

   }
   std::cout << "excavator position reached!!!!!" << std::endl;
   excavatorInRightPosition_ = true;
   ros::spinOnce();
   publish(w_q_EE_ref_, w_r_EE_ref_, moveKraftPosition_, moveKraftOrientation_, stayVertical_);
   stayVertical_ = false;
 } // m545CommandsPublisher


 ////////////////////////////////////////////////////////////////////////////////
 ///////////////////////  Autonomous Controller /////////////////////////////////
 ////////////////////////////////////////////////////////////////////////////////

 void KraftController::kraftStonesCallback(const kraft_msgs::StonesPose::ConstPtr& stonesmsg){
     for (int i = 0; i < NoStones; i++)
     {
       stonesPosition[i](0) = stonesmsg->poses[i].Stone_pose.position.x;
       stonesPosition[i](1) = stonesmsg->poses[i].Stone_pose.position.y;
       stonesPosition[i](2) = stonesmsg->poses[i].Stone_pose.position.z;

       stonesQuat[i].setX(stonesmsg->poses[i].Stone_pose.orientation.x);
       stonesQuat[i].setY(stonesmsg->poses[i].Stone_pose.orientation.y);
       stonesQuat[i].setZ(stonesmsg->poses[i].Stone_pose.orientation.z);
       stonesQuat[i].setW(stonesmsg->poses[i].Stone_pose.orientation.w);

       double yaw, pitch, roll;
       tf::Matrix3x3(stonesQuat[i]).getEulerYPR(yaw,pitch,roll);
       stonesEuler[i](0) = roll;
       stonesEuler[i](1) = pitch;
       stonesEuler[i](2) = yaw;
     }
 } // kraftStonesCallback

 // function to move the end-effector to the given position. Orientation is kept vertical.
 bool KraftController::moveToPosition(Eigen::Vector3d& position){
   std::cout << "moveToPosition started" << std::endl;
   // set margin for reached position
   float epsilon = 0.0005;
   bool position_reached = false;
   Eigen::Vector3d Difference;


   while(!position_reached){
     Difference =  w_r_EE_ - position;

     if(Difference.norm() > epsilon)
     {
       w_r_EE_ref_ = position;
       w_q_EE_ref_.setW(1);
       w_q_EE_ref_.setX(0);
       w_q_EE_ref_.setY(0);
       w_q_EE_ref_.setZ(0);
     }
     else
     {
       position_reached = true;
     }
     ros::spinOnce();
     if(stopAutonomous_){
       position_reached = false;
       break;
     }
     publish(w_q_EE_ref_,w_r_EE_ref_,moveKraftPosition_,moveKraftOrientation_,stayVertical_);
   }
   return position_reached;
 } // moveToPosition

 // change the orientation to the orientaiton of the i th stone
 bool KraftController::changeToOrientaion(int whichStone){
   std::cout << "changeToOrientation started" << std::endl;
   bool orientation_reached = false;
   moveKraftPosition_ = true;
   stayVertical_ = false;
   moveKraftOrientation_ = true;
   float epsilon = 0.005;

   double yaw,pitch,roll;

   while (!orientation_reached)
   {
     if((w_euler_EE_(2) > stonesEuler[whichStone](2)+0.01) || (w_euler_EE_(2) < stonesEuler[whichStone](2)-0.01))
     {
       std::cout << "yaw angle of the stone" << stonesEuler[whichStone](2) << std::endl;
       yaw = stonesEuler[whichStone](2);
       pitch = 0;
       roll = 0;
       w_q_EE_ref_.setRPY(roll, pitch, yaw);
     }
     else
     {
       orientation_reached = true;
     }
     ros::spinOnce();
     publish(w_q_EE_ref_,w_r_EE_ref_,moveKraftPosition_, moveKraftOrientation_,stayVertical_);
   }
   moveKraftPosition_ = true;
   moveKraftOrientation_ = true;
   stayVertical_ = false;

   return orientation_reached;
 }

// start of the actual autonomous control mode.
 void KraftController::autonomousControl(){
   std::cout << "autonomous Control started :):)" << std::endl;
   running_ = true;
   moveKraftPosition_ = true;
   stayVertical_ = true;
   moveKraftOrientation_ = true;

   for (int i = 0; i < NoStones; i++)
   {
     std::cout << "gonna take stone " << i << "next" << std::endl;
     if(stopAutonomous_)
     {
       std::cout << "I stopped autonomous control, because YOU pressed the button. DON'T DO THAT!!!" << std::endl;
       running_ = false;
       break;
     }
     stone_Wp2_[i] << stonesPosition[i](0), stonesPosition[i](1),stonesPosition[i](2)+0.8;
     stone_Wp3_[i] << stonesPosition[i](0), stonesPosition[i](1), stonesPosition[i](2)+0.4;
     stone_Wp4_[i] << stonesPosition[i](0), stonesPosition[i](1), stonesPosition[i](2)+0.25;
     stone_Wp5_[i] << stonesPosition[i](0), stonesPosition[i](1), stonesPosition[i](2)+0.17;

   bool position_reached;
   bool orientation_reached;
   bool successful_catch;

   position_reached = moveToPosition(stone_Wp1_AS_);

   std::cout << "Above Shovel" << std::endl;
//   sleep(0.5);

   position_reached = moveToPosition(stone_Wp2_[i]);

   std::cout << "I am at the second Waypoint above stone" << std::endl;
//   sleep(2);

   position_reached = moveToPosition(stone_Wp3_[i]);

   orientation_reached = changeToOrientaion(i);

   std::cout << "Such a wanker, now i had to turn myself" << std::endl;

   // open Gripper start

      successful_catch = false;
      while(!successful_catch)
      {
        successful_catch = gripperCommandPublisher("open");

      if(successful_catch)
      {
        std::cout << "Gripper opened successfully" << std::endl;
      }
      else
      {
        successful_catch = gripperCommandPublisher("open");
      }
      }
   // open Gripper finished
//   sleep(1);

   position_reached = moveToPosition(stone_Wp4_[i]);

   std::cout << "I reached the third Waypoint. I AM GONNA GET THAT STONE NOW" << std::endl;

   position_reached = moveToPosition(stone_Wp5_[i]);

//   sleep(2);

   // close Gripper start

      successful_catch = false;

      successful_catch = gripperCommandPublisher("close");

      if(successful_catch)
      {
        std::cout << "Gripper closed successfully and has the stone" << std::endl;
      }

      while(!successful_catch)
      {
        ros::spinOnce();
        stone_Wp2_[i] << stonesPosition[i](0), stonesPosition[i](1),stonesPosition[i](2)+0.8;
        stone_Wp3_[i] << stonesPosition[i](0), stonesPosition[i](1), stonesPosition[i](2)+0.4;
        stone_Wp4_[i] << stonesPosition[i](0), stonesPosition[i](1), stonesPosition[i](2)+0.25;
        stone_Wp5_[i] << stonesPosition[i](0), stonesPosition[i](1), stonesPosition[i](2)+0.17;
        position_reached = moveToPosition(stone_Wp3_[i]);
        position_reached = moveToPosition(stone_Wp4_[i]);
        position_reached = moveToPosition(stone_Wp5_[i]);
        successful_catch = gripperCommandPublisher("close");
        if (stopAutonomous_)
        {
          break;
        }
      }
   // close Gripper ended

   std::cout << "got the FUCKING STONE, YES!!!!!!!!!!!!!!!!!!!!!" << std::endl;
   sleep(1);

   position_reached = moveToPosition(stone_Wp3_[i]);

   stayVertical_ = true;

   position_reached = moveToPosition(stone_Wp3_[i]);
   sleep(0.5);

   position_reached = moveToPosition(stone_Wp2_[i]);
   sleep(0.5);

   position_reached = moveToPosition(stone_Wp1_AS_);
   sleep(1);

   // open Gripper started

      successful_catch = false;
      while(!successful_catch)
      {
        successful_catch = gripperCommandPublisher("open");

      if(successful_catch)
      {
        std::cout << "Gripper opened successfully" << std::endl;
      }
      else
      {
        successful_catch = gripperCommandPublisher("open");
      }
      }
   // open Gripper finished

      sleep(1);

   std::cout << " I PUT THE STONE IN THE SHOVEL FOR YOU " << std::endl;

   ros::spinOnce();
   } // while loop ended

   running_ = false;
   std::cout << "autonomous control ended successfully :):)" << std::endl;
 }

 ///////////////////////////////////////////////////////////////////////////////////////
 ////////////////////////////////// Gripper Commands ///////////////////////////////////
 ///////////////////////////////////////////////////////////////////////////////////////

void KraftController::kraftGripperStateCallback(const kraft_msgs::GripperState::ConstPtr& msg){
  gripperCylinderPos_ = msg->gripperState.position[0];
  gripperCylinderVel_ = msg->gripperState.velocity[0];
  gripperCylinderForce_ = msg->gripperState.effort[0];
}

// opens or closed respectively to the given string. closes the gripper with a certain velocity until force is too much or lower limit is reached. Gives back bool in order to check whether something is grabbed.
bool KraftController::gripperCommandPublisher(std::string gripperDesiredState){
  bool successfull_catch = false;

  if(gripperDesiredState == "close")
  {
    std::cout << "I will close the gripper now" << std::endl;
    while(gripperCylinderPos_ < 0.015){
      std::cout << "gripperCylinderPos_ = " << gripperCylinderPos_ << std::endl;
      gripperCommandmsg_.mode = 2;
      gripperCommandmsg_.referenceCylinderVelocity = 0.05;
      gripperCommandmsg_.referenceCylinderForce = 50;
      gripperCommandmsg_.referenceCylinderPosition = 0.0;
      gripperActuatorPub_.publish(gripperCommandmsg_);
      ros::spinOnce();
      if (gripperCylinderPos_ > -0.01)
      {
      if (gripperCylinderVel_ < 0.001){
        std::cout << "force is too much, picked a stone" << std::endl;
        gripperCommandmsg_.mode = 3;
        gripperCommandmsg_.referenceCylinderVelocity = 0.0;
        gripperCommandmsg_.referenceCylinderForce = 200;
        gripperCommandmsg_.referenceCylinderPosition = 0.0;
        gripperActuatorPub_.publish(gripperCommandmsg_);
        successfull_catch = true;
        break;
      }
    }
    }
    if(!successfull_catch)
    {
    while(gripperCylinderVel_ > 0.01)
    {
      std::cout << "gripperCylinderPos_ set to 0.018 " << std::endl;
    gripperCommandmsg_.mode = 1;
    gripperCommandmsg_.referenceCylinderVelocity = 0.0;
    gripperCommandmsg_.referenceCylinderForce = 0.0;
    gripperCommandmsg_.referenceCylinderPosition = 0.018;
    gripperActuatorPub_.publish(gripperCommandmsg_);
    ros::spinOnce();
    }
  }
    std::cout << "gripper is closed" << std::endl;
    }

  if(gripperDesiredState == "open")
  {
    std::cout << "I am gonna open the gripper for you" << std::endl;

    bool continue_ = true;
    while(gripperCylinderPos_ > -0.01)
    {
      std::cout << "gripperCylinderPos_ = " << gripperCylinderPos_ << std::endl;
        gripperCommandmsg_.mode = 2;
        gripperCommandmsg_.referenceCylinderVelocity = -0.05;
        gripperCommandmsg_.referenceCylinderForce = 0.0;
        gripperCommandmsg_.referenceCylinderPosition = 0.0;
        gripperActuatorPub_.publish(gripperCommandmsg_);
        ros::spinOnce();
    }

    while(gripperCylinderVel_  > 0.005)
    {
      gripperCommandmsg_.mode = 1;
      gripperCommandmsg_.referenceCylinderVelocity = 0.0;
      gripperCommandmsg_.referenceCylinderForce = 0.0;
      gripperCommandmsg_.referenceCylinderPosition = -0.015;
      gripperActuatorPub_.publish(gripperCommandmsg_);
      ros::spinOnce();
    }
    std::cout << "open has ended" << std::endl;
    successfull_catch = true;
  }
  return successfull_catch;
}

}; // kraft_controller

