// to use nodewrap, the node.cpp file must be added as executable and the cpp must be added as library.
// Then the executable and the library must be linked



#include "ros/ros.h"
#include <roscpp_nodewrap/Node.h>
#include "../include/kraft_controller/KraftController.hpp"



int main(int argc, char **argv){

  ros::init(argc, argv, "kraft_controller");

  std::cout << "kraft_controller executed" << std::endl;

  nodewrap::Node<kraft_controller::KraftController> node;

  ros::spin();

  return 0;
}
