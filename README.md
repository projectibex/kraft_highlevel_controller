#High-Level Controller for a Kraft Predator

##Installation

###Dependencies
- [catkin_simple](https://github.com/catkin/catkin_simple)
- [kraft_description](https://bitbucket.org/projectibex/kraft_common)
- [kraft_msgs](https://bitbucket.org/projectibex/kraft_common)
- [kraft_utils](https://bitbucket.org/projectibex/kraft_common)
- [kraft_actuator](https://bitbucket.org/projectibex/kraft_common)
- [kraft_model](https://bitbucket.org/projectibex/kraft_common)
- [m545_vortex](https://bitbucket.org/ethz-asl-lr/m545_vortex) (branch: feature/kraft)
- [roco](https://bitbucket.org/ethz-asl-lr/roco)
- [kindr](https://github.com/ethz-asl/kindr)
- [romo](https://bitbucket.org/ethz-asl-lr/romo) (branch: feature/kraft)
- [rbdl](https://bitbucket.org/ethz-asl-lr/rbdl) ("hg up master")
- [robot_utils](https://bitbucket.org/ethz-asl-lr/robot_util) (place a CATKIN_IGNORE in robot_utils_sc package)
- [message_logger](https://bitbucket.org/ethz-asl-lr/message_logger)

###Build
Clone this repository into your catkin workspace.
Set Catkin Testing off with the follwing catkin config command:
catkin config --cmake-args -DCMAKE_BUILD_TYPE=release -DCATKIN_ENABLE_TESTING=OFF

```
#!bash
cd catkin_ws/src
git clone git@bitbucket.org:projectibex/kraft_highlevel_controller.git
cd ../
```


##Usage
## Launch
To launch the high-level controller:

`roslaunch kraft_highlevel_controller kraft_highlevel_controller.launch`

## ROS Node

Using [rospp-nodewrap](https://github.com/ethz-asl/roscpp-nodewrap) the published/subscribed topic names can be edited. Moreover the highlevel controller can be customized using several parameters. The default highlevel controller can be customized to your application using the  config files located at ```kraft_highlevel_controller/kraft_highlevel_controller/params```.
This repository contains two seperate nodes. One is the kraft_highlevel_controller node which contains the inverse kinematic and sends joint velocity command to the vortex simulation and the other is the kraft_controller which contains the manual and autonomous control mode and sends reference position and orientation to the kraft_highlevel_controller. Furthermore it sends gripper commands directly to the m545_vortex node. 

## Kraft_highlevel_controller

#### Subscribed Topics

* **`kraft_state`** ([kraft_msgs/KraftState])

    The robot state.

* **`kraft_joystick`** ([sensor_msgs/Joy])

    The joystick commands.

* **`gripper_base_reference`** ([kraft_msgs/GripperBaseReference])

    Reference position and orientation of the end-effector.

#### Published Topics

* **`kraft_actuator_commands`** ([kraft_msgs/KraftActuatorCommands])

    Kraft actuator commands.

* **`gripper_base_pose`** ([kraft_msgs/GripperBasePose])

     Actual position and orientation of the end-effector caculated with the joint states and the URDF file


#### Parameters


* **`controller/time_step`** (double, default: "0.01")
 
    Time step in seconds between two consecutive controller updates.

* **`controller/joystick`** (string, default: "CAD")

## Kraft_controller

#### Subscribed Topics

* **`kraft_joystick`** ([sensor_msgs/Joy])

     The joystick commands

* **`gripper_base_pose`** ([kraft_msgs/GripperBasePose])

     Actual position and orientation of the end-effector caculated with the joint states and the URDF file

* **`m545_state`** ([m545_msgs/M545State])

     State of the M545 including all the joint position, volecities and efforts

* **`kraft_stones_poses`** ([m545_msgs/StonesPoses])

     Position and orientation of all the stones added to the simulation.

* **`kraft_shovel_pose`** ([kraft_msgs/ShovelPose])

     Position and orientation of the shovel of the excvator to calculate the first waypoint for every stone.

* **`gripper_state`** ([kraft_msgs/GripperState])

     State of the gripper of the manipualtor.

#### Published Topics

* **`gripper_base_reference`** ([kraft_msgs/GripperBaseReference])

     Reference position and orientation of the end-effector either calculated with the joystick inputs or the autonomous control mode.

* **`m545_actuator_commands`** ([m545_msgs/M545ActuatorCommands])

     Actuator commands for the excavator to put the shovel to the ground.

* **`kraft_gripper_commands`** ([kraft_msgs/GripperCommand])

     Commands for the gripper to open and close it directly sent to the m545_vortex node.