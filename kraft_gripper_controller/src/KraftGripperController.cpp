#include "kraft_gripper_controller/KraftGripperController.hpp"


namespace kraft_gripper_controller{

KraftGripperController::KraftGripperController():
    gripperCommandSrv_(),
    gripperCylinderPos(0.0),
    gripperCylinderVel(0.0),
    gripperCylinderForce(0.0)
{
}

KraftGripperController::~KraftGripperController(){}

void KraftGripperController::init(){
  initializeServices();
  initializeSubscriber();
  initializePublisher();
}

void KraftGripperController::cleanup(){}

void KraftGripperController::initializeServices(){
  gripperCommandSrv_ = advertiseService("kraft_gripper_command", "/kraft_gripper_command", &KraftGripperController::gripperCommandPublisher);
}

void KraftGripperController::initializeSubscriber(){
  kraftStateSub_ = subscribe("gripper_state","/gripper_state",1,&KraftGripperController::kraftStateCallback);
}

void KraftGripperController::initializePublisher(){
  gripperActuatorPub_ = advertise<kraft_msgs::GripperCommand>("kraft_gripper_commands", "/kraft_gripper_command",1);
}

void KraftGripperController::kraftStateCallback(const kraft_msgs::GripperState::ConstPtr& msg){
  gripperCylinderPos = msg->gripperState.position[0];
  gripperCylinderVel = msg->gripperState.velocity[0];
  gripperCylinderForce = msg->gripperState.effort[0];
}

bool KraftGripperController::gripperCommandPublisher(kraft_msgs::GripperCommandService::Request  &req, kraft_msgs::GripperCommandService::Response &res){
  if(req.state == "open")
  {
    std::cout << "open" << std::endl;
    while(gripperCylinderPos < (0.067)){
      gripperCommand.mode = 2;
      gripperCommand.referenceCylinderVelocity = 0.5;
      gripperCommand.referenceCylinderForce = 100;
      gripperCommand.referenceCylinderPosition = 0.0;
      gripperActuatorPub_.publish(gripperCommand);
      ros::spinOnce();
    }

    gripperCommand.mode = 0;
    gripperCommand.referenceCylinderVelocity = 0.0;
    gripperCommand.referenceCylinderForce = 0.0;
    gripperCommand.referenceCylinderPosition = 0.0;
    gripperActuatorPub_.publish(gripperCommand);
  }

  if(req.state == "close")
  {
    std::cout << "closed" << std::endl;

    bool continue_ = true;
    while(continue_){
      if(gripperCylinderForce > 100){
        continue_ = false;
      }
      gripperCommand.mode = 3;
      gripperCommand.referenceCylinderVelocity = 0.0;
      gripperCommand.referenceCylinderForce = -100;
      gripperCommand.referenceCylinderPosition = 0.0;
      gripperActuatorPub_.publish(gripperCommand);
      ros::spinOnce();
      if(gripperCylinderPos < (-0.026)){
        continue_ = false;
      }
    }

    gripperCommand.mode = 0;
    gripperCommand.referenceCylinderVelocity = 0.0;
    gripperCommand.referenceCylinderForce = 0.0;
    gripperCommand.referenceCylinderPosition = 0.0;
    gripperActuatorPub_.publish(gripperCommand);
  }

  res.reached = true;

  return true;
}
} // namespace kraft_gripper_controller
